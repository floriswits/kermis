import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RunKermis {
    Scanner scanner;
    List<Attractie> attracties = new ArrayList<>();
    Kassa kassa = new Kassa();

    public RunKermis(Scanner invoer) {
        this.scanner = invoer;
        this.attracties.add(new Botsautos(1));
        this.attracties.add(new Spin(2));
        this.attracties.add(new Spiegelpaleis(3));
        this.attracties.add(new Spookhuis(4));
        this.attracties.add(new Hawaii(5));
        this.attracties.add(new Ladderklimmen(6));
    }

    public void draaien() throws Exception {
        System.out.println("Voer getal in (1-6) om attractie te draaien.");
        for (Attractie a : attracties) {
            System.out.println(">>"+a.naam + ": " + a.getal);
        }
        System.out.println("Voer 'o' in voor kasbalans.\nVoer 'k' in voor kaartverkoop.\nVoer 'b' in voor belastinginspecteur.\nSluit applicatie met getal 0.");

        String invoer = this.scanner.nextLine();
        if (Integer.valueOf(invoer) > this.attracties.size()) {
            System.out.println("INVOER ONGELDIG!!!\n");
            draaien();
        }
        if (!invoer.equals("0")) {

            if (invoer.equals("o")) {
                System.out.println("Kasbalans: " + kassa.getBalans());
                draaien();
            }
            if (invoer.equals("k")) {
                System.out.println("Verkochte kaartjes totaal: " + kassa.getVerkochteKaartjesTotaal());
                draaien();
            }
            if (invoer.equals("b")) {
                System.out.println("BelastingInspecteur komt langs!");
                kassa.addInspecteursBezoek();
                System.out.println("Kasbalans: " + kassa.getBalans());
                for (Attractie a : attracties) {
                    if (a instanceof GokAttractie) {
                        BelastingInspecteur belastingInspecteur = new BelastingInspecteur();
                        double belasting = belastingInspecteur.haalBelasting(a);
                        kassa.add(-belasting);
                        a.addOmzet(-belasting);
                        System.out.println("Gokattractie: " + a.getNaam());
                        System.out.println("Omzet attractie: " + a.getOmzet());

                        System.out.println("Opgehaalde belasting: " + belastingInspecteur.opgehaaldeBelasting);
                        System.out.println("Nieuw kasbalans: " + kassa.getBalans());
                        System.out.println("Nieuwe omzet attractie: " + a.getOmzet());
                    }
                }
                System.out.println("Aantal inspecties: " + kassa.getInspecties());
                draaien();
            }
            Attractie attractie;
            attractie = attracties.get(Integer.parseInt(invoer) - 1);
            System.out.println("Je hebt gekozen voor draaien van attractie: " + attractie.getNaam());
            if (attractie instanceof RisicoRijkeAttractie) {
                if (attractie.getVerkochteKaartjes() >= ((RisicoRijkeAttractie) attractie).draailimiet) {
                    System.out.println("EXCEPTIONNN limiet: " + attractie.getNaam());
                    throw new Exception("limiet overschreden");
                }
            }

            attractie.addOmzet(attractie.getPrijs());
            kassa.add(attractie.getPrijs());
            attractie.addVerkochtKaartje();
            kassa.addVerkochtKaartje();

            System.out.println("Omzet attractie: " + attractie.getOmzet());
            System.out.println("Verkochte kaartjes attractie: " + attractie.getVerkochteKaartjes());
            System.out.println("Kasbalans: " + kassa.getBalans());
            System.out.println("Verkochte kaartjes totaal: " + kassa.getVerkochteKaartjesTotaal());

            draaien();
        } else {
            System.out.println("Eind applicatie");
        }
    }
}