public class Botsautos extends Attractie {

    private Double prijs = 2.50;
    private String naam = "Botsauto's";

    public Botsautos(int getal) {
        super(getal);
        super.naam = naam;
        super.prijs = prijs;
    }
}