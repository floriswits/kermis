public class Kassa {
    private double balans;
    private int verkochteKaartjesTotaal;
    private int aantalInspecteursbezoekjes;

    public double getBalans() {
        return this.balans;
    }

    public double add(double geld) {
       return balans += geld;
    }

    public int getVerkochteKaartjesTotaal() {
        return this.verkochteKaartjesTotaal;
    }

    public void addVerkochtKaartje() {
        verkochteKaartjesTotaal++;
    }

    public int getInspecties(){
        return this.aantalInspecteursbezoekjes;
    }

    public void addInspecteursBezoek() {
        this.aantalInspecteursbezoekjes++;
    }
}