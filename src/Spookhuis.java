public class Spookhuis extends Attractie{
    final double prijs = 3.20;
    private String naam = "Spookhuis";

    public Spookhuis(int getal) {
        super(getal);
        super.naam = naam;
        super.prijs = prijs;
    }
}
