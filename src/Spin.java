public class Spin extends RisicoRijkeAttractie implements GokAttractie {

    private Double prijs = 2.25;
    private String naam = "Spin";
    public final int draailimiet = 5;

    public Spin(int getal) {
        super(getal);
        super.naam = naam;
        super.prijs = prijs;
        super.draailimiet = draailimiet;
    }

    @Override
    public double kansspelbelastingBetalen() {
        double belasting = (this.getOmzet() * 0.3);
        return belasting;
    }
}