
public class Spiegelpaleis extends Attractie{
    private double prijs = 2.75;
    private String naam = "Spiegelpaleis";

    public Spiegelpaleis(int getal) {
        super(getal);
        super.naam = naam;
        super.prijs = prijs;
    }
}