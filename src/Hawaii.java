public class Hawaii extends RisicoRijkeAttractie{
    private Double prijs = 2.90;
    private String naam = "Hawaii";
    public final int draailimiet = 10;

    public Hawaii(int getal) {
        super(getal);
        super.naam = naam;
        super.prijs = prijs;
        super.draailimiet = draailimiet;
    }

}
