public class Ladderklimmen extends Attractie implements GokAttractie {
    private Double prijs = 5.0;
    private String naam = "Ladderklimmen";


    public Ladderklimmen(int getal) {
        super(getal);
        super.naam = naam;
        super.prijs = prijs;
    }

    @Override
    public double kansspelbelastingBetalen() {
        double belasting = (this.getOmzet() * 0.3);
        return belasting;
    }
}