public class Attractie {

    public int getal;
    public String naam;
    public double prijs;
    private int oppervlakte;
    private double omzet;
    private int verkochteKaartjes;

    public Attractie(int getal) {
        this.getal = getal;
    }

    public int getVerkochteKaartjes() {
        return this.verkochteKaartjes;
    }

    public int addVerkochtKaartje() {
       return verkochteKaartjes+=1;
    }

    public double getPrijs() {
        return this.prijs;
    }

    public String getNaam() {
        return this.naam;
    }

    public double addOmzet(double geld) {
        return omzet += geld;
    }

    public double getOmzet() {
        return this.omzet;
    }

}